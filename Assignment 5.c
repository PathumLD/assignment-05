#include<stdio.h>
#define  PC 500 //performance cost
#define  AC 3   //attendee cost


int attendees(int price);
int income(int price);
int cost(int price);
int profit(int price);


int	attendees(int price){
	return 120-(price-15)/5*20;
}

int income(int price){
	return attendees(price)*price;
}

int cost(int price){
	return attendees(price)*AC+PC;
}

int profit(int price){
	return income(price)-cost(price);
}

int main(){
	int price;
	printf("\n\nRelationship between ticket price and profit: \n\n");
	for(price=5;price<50;price+=5)
	{
		printf("Ticket Price = Rs.%d \t\t Profit = Rs.%d\n",price,profit(price));
        printf("\n");
    }
        printf("Highest profit made by ticket price is Rs.25\n");
		return 0;
	}
